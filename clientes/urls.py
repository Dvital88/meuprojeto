from django.urls import path
from .views import persons_list, persons_create, persons_update, persons_delete

urlpatterns = [
  path('list/', persons_list, name='person_list'),
  path('create/', persons_create, name='person_create'),
  path('update/<int:id>', persons_update, name='person_update'),
  path('delete/<int:id>', persons_delete, name='person_delete')
]