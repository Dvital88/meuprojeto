from django.shortcuts import render, redirect, get_object_or_404
from .models import Person
from .forms import PersonForm

# Create your views here.


def persons_list(request):
    persons = Person.objects.all()

    return render(request, 'person_list.html', {'persons': persons})

def persons_create(request):
    form = PersonForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('person_list')

    return render(request, 'person_create.html', {'form': form})


def persons_update(request, id):
    person = get_object_or_404(Person, pk=id)

    form = PersonForm(request.POST or None, instance=person)

    if form.is_valid():
        form.save()
        return redirect('person_list')

    return render(request, 'person_create.html', {'person': person, 'form': form})


def persons_delete(request, id):
    person = get_object_or_404(Person, pk=id)

    form = PersonForm(request.POST or None, instance=person)

    if request.method == 'POST':
        person.delete()
        return redirect('person_list')

    return render(request, 'person_delete.html', {'person': person, 'form': form})